package com.feedzai.chunyang.baccarats;

public class Rules {
	
	public static boolean playerRule(int bankerPoints, int playerPoints) {
		if (bankerPoints >= 8 || playerPoints >= 8)
			return false;
		if (playerPoints >= 6)
			return false;
		else
			return true;
	}

	public static boolean bankerRule(int bankerPoints, int playerPoints, int playerthirdcard) {
		if (bankerPoints >= 8 || playerPoints >= 8)
			return false;
		if (bankerPoints == 7)
			return false;
		// ---------------------------------------------------------
		// case if the player has not drawn a third card
		// ---------------------------------------------------------
		if (playerPoints >= 6) {
			if (bankerPoints == 6)
				return false;
			else
				return true;
		}
		// ---------------------------------------------------------
		// case if the player has drawn a third card; implement
		// the third-card rule
		// ---------------------------------------------------------
		switch (bankerPoints) {
		case 0:
		case 1:
		case 2:
			return true;
		case 3:
			if (playerthirdcard != 8)
				return true;
			break;
		case 4:
			if (playerthirdcard >= 2 && playerthirdcard <= 7)
				return true;
			break;
		case 5:
			if (playerthirdcard >= 4 && playerthirdcard <= 7)
				return true;
			break;
		case 6:
			if (playerthirdcard == 6 || playerthirdcard == 7)
				return true;
			break;
		}
		return false;
	}
}
