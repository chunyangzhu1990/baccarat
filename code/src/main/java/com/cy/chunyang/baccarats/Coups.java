package com.feedzai.chunyang.baccarats;

import java.util.ArrayList;
import java.util.List;

public class Coups {

	private Shoe shoe;
	private int shufflePoint;
	public boolean print;

	public Coups(int p) {
		shoe = new Shoe(p);
		this.shufflePoint = shoe.getSizeofShoe() / 2;
		shoe.shuffle();
		print = true;
	}

	public int start() {
		if (shoe.getNumofCardsLeft() <= shufflePoint)
			shoe.shuffle();
		Hand banker = new Hand();
		Hand player = new Hand();
		banker.takeACard(shoe.draw());
		player.takeACard(shoe.draw());
		banker.takeACard(shoe.draw());
		player.takeACard(shoe.draw());

		int bankerpoints = banker.handPoints();
		int playerpoints = player.handPoints();

		int playerThirdCardValue = -1;
		if (Rules.playerRule(bankerpoints, playerpoints)) {
			player.takeACard(shoe.draw());
			playerpoints = player.handPoints();
			playerThirdCardValue = Hand.cardPoints(player.hand.get(2));
		}

		if (Rules.bankerRule(bankerpoints, playerpoints, playerThirdCardValue)) {
			banker.takeACard(shoe.draw());
			bankerpoints = banker.handPoints();
		}

		if (print)
			printGame(banker, player);
		return bankerpoints - playerpoints;
	}

	public static void printGame(Hand bankerHand, Hand playerHand) {
		System.out.println("Player Hand");
		System.out.println("First card:  " + playerHand.hand.get(0) + "");
		System.out.println("Second card: " + playerHand.hand.get(1) + "");
		System.out.print("Third card:  ");
		if (playerHand.hand.size() == 3)
			System.out.print(playerHand.hand.get(2) + "");
		else
			System.out.print("No card drawn       ");
		System.out.println("Banker Hand");
		
		System.out.println("First card:  " + playerHand.hand.get(0) + "");

		System.out.println("Second card: " + playerHand.hand.get(1) + "");
		if (bankerHand.hand.size() == 3)
			System.out.println(bankerHand.hand.get(2) + "");
		else
			System.out.println("No card drawn");

		int playerValue = playerHand.handPoints();
		int bankerValue = bankerHand.handPoints();
		if (playerValue > bankerValue)
			System.out.println("---Player wins---");
		else if (playerValue < bankerValue)
			System.out.println("---Banker wins---");
		else
			System.out.println("---tie---");
	}

}

class Hand {
	List<Card> hand;

	public Hand() {
		this.hand = new ArrayList<Card>();
	}

	public void takeACard(Card card) {
		hand.add(card);
	}

	public static int cardPoints(Card card) {
		int point = card.getRank().getValue();
		if (point > 9)
			point = 0;
		return point;
	}

	public int handPoints() {
		int point = 0;
		for (int i = 0; i < hand.size(); i++)
			point += cardPoints(hand.get(i));
		return point % 10;
	}
}