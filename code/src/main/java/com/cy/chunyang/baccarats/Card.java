package com.feedzai.chunyang.baccarats;

public class Card {

	public enum Rank {
		ACE(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(11), QUEEN(
				12), KING(13);

		private int value;
		private Rank(final int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
	}

	public enum Suit {
		SPADE, HEART, DIAMON, CLUB
	}

	private Rank rank;
	private Suit suit;

	public Card(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}

	public Rank getRank() {
		return rank;
	}

	public Suit getSuit() {
		return suit;
	}

	public String toString() {
		return rank + "of" + suit;
	}

	public static void main(String[] args) {
		Rank rank = Rank.ACE;
		Suit suit = Suit.CLUB;
		Card card = new Card(rank, suit);
		System.out.println(card.toString());
	}
}
