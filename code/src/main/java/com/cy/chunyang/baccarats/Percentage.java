package com.feedzai.chunyang.baccarats;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;

public class Percentage {
	
	
	@SuppressWarnings("unchecked")
	
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/percentage")
	@GET
	public String getPercent(){
		
		Coups baccarat = new Coups(8);
		int coups = 1000000;
		if (coups > 20)
			baccarat.print = false;

		int bankerWins = 0;
		int playerWins = 0;
		int tie = 0;
		for (int i = 0; i < coups; i++) {
			int result = baccarat.start();
			if (result > 0)
				bankerWins++;
			else if (result < 0)
				playerWins++;
			else
				tie++;
				
		}

		double bankerPercent = bankerWins*100.0/coups;
	    double playerPercent = playerWins*100.0/coups;
	    double tiePercent = tie*100.0/coups;
	    
	    JSONObject js  = new JSONObject();
	    js.put("bankerPercent", bankerPercent);
	    js.put("playerPercent", playerPercent);
	    js.put("tiePercent", tiePercent);
		return js.toString();
	}

	public static void main(String[] args) {
		Coups baccarat = new Coups(8);
		int coups = 1000000;
		if (coups > 20)
			baccarat.print = false;

		int bankerWins = 0;
		int playerWins = 0;
		int tie = 0;
		for (int i = 0; i < coups; i++) {
			int result = baccarat.start();
			if (result > 0)
				bankerWins++;
			else if (result < 0)
				playerWins++;
			else
				tie++;
				
		}

		double bankerPercent = bankerWins*100.0/coups;
	    double playerPercent = playerWins*100.0/coups;
	    double tiePercent = tie*100.0/coups;
		System.out.println("No of games played = " + coups);
		System.out.println("Winning percentage of banker = " + bankerPercent);
		System.out.println("Winning percentage of player = " + playerPercent);
		System.out.println("Tie percentage = " + tiePercent);
	}

}
