package com.feedzai.chunyang.baccarats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Shoe {

	final static int numSuit = 4; // no of suits
	final static int cardType = 13; // no of values in each suit
	final static int sizeDeck = 52; // no of cards in each pack

	private List<Card> cards;
	private int totalCardsNum; // no of cards in the deck
	private int dealtCardsNum; // no of cards dealt from the deck

	public Shoe(int p) {
		totalCardsNum = p * sizeDeck;
		dealtCardsNum = 0;
		cards = new ArrayList<Card>();
		Card.Rank[] ranks = Card.Rank.values();
		Card.Suit[] suits = Card.Suit.values();
		for (int i = 0; i < p; i++) {
			for (Card.Rank rank : ranks) {
				for (Card.Suit suit : suits) {
					cards.add(new Card(rank, suit)); 
				}
			}
		}
	}

	public Card draw() {
		return cards.get(dealtCardsNum++);
	}

	public void shuffle() {
		Collections.shuffle(cards);
		dealtCardsNum = 0;
	}


	public int getSizeofShoe() {
		return totalCardsNum;
	}

	public int getNumofCardsLeft() {
		return (totalCardsNum - dealtCardsNum);
	}

	public static void main(String[] args) {
		Shoe d = new Shoe(8);
		System.out.println(d.cards.size());
	}

}
