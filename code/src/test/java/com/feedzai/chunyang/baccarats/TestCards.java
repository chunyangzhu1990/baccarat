package com.feedzai.chunyang.baccarats;
import static org.junit.Assert.*;

import org.junit.Test;

import com.feedzai.chunyang.baccarats.Card.Rank;
import com.feedzai.chunyang.baccarats.Card.Suit;

public class TestCards {
	
@Test
public void testCard(){
	Rank rank = Rank.ACE;
	Suit suit = Suit.CLUB;
	Card card = new Card(rank, suit);
	assertTrue(card.getRank().getValue() == 1);
	assertTrue(card.getSuit().equals(suit));
}
@Test
public void testShoe(){
	Shoe d = new Shoe(8);
	assertTrue(d.getSizeofShoe() == 52*8);
}



}
